**Movies and Series MEAN Stack web-app.**

```
[TODO]
Introduce the app as a micro-service with a RESTful API,
try to explain this concepts.
```

---

## MEAN Stack ##
 **MongoDB, Express, AngularJS, Node**

A [MEAN Stack](https://www.mongodb.com/blog/post/the-modern-application-stack-part-1-introducing-the-mean-stack) web app.

* [MongoDB](https://www.mongodb.com/what-is-mongodb)  - Document Database

* [Express](https://expressjs.com/)  - Web Application Framework

* [AngularJS](https://angularjs.org/)  - Front-end Framework

* [Node](https://nodejs.org/en/) - JavaScript runtime environment

** Other **

* [Seneca](http://senecajs.org/)  - Micro-services Toolkit

* [Multer](https://github.com/expressjs/multer) -  Node.js middleware for handling multipart/form-data


---

